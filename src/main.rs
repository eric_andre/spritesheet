extern crate piston;
extern crate graphics;
extern crate glutin_window;
extern crate opengl_graphics;

use std::path::Path;

use piston::window::WindowSettings;
use piston::event_loop::*;
use piston::input::*;
use glutin_window::GlutinWindow as Window;
use opengl_graphics::{ GlGraphics, OpenGL };

extern crate spritesheet;
use spritesheet::{ Animation, Orientation };

pub struct App<'a> {
    gl: GlGraphics,
    spritesheet: spritesheet::SpriteSheet<'a>,
    spritesheet_two: spritesheet::SpriteSheet<'a>,
    walk_sheet: spritesheet::SpriteSheet<'a>,
    raccoon_sheet: spritesheet::SpriteSheet<'a>,
    megaman_sheet: spritesheet::SpriteSheet<'a>,
    walk_x: f64,
    racc_x: f64,
    mega_x: f64,
    mega_y: f64,
    mega_vel: f64,
    anim: f64,
    dirty: bool,
}

impl<'a> App<'a> {
    fn render(&mut self, args: &RenderArgs) {
        use graphics::*;

        const GREEN: [f32; 4] = [0.0, 1.0, 0.0, 1.0];
        const RED: [f32; 4] = [1.0, 0.0, 0.0, 1.0];

        let square = rectangle::square(0.0, 0.0, 50.0);

        let ball = &mut self.spritesheet;
        let ball2 = &mut self.spritesheet_two;
        let walk = &mut self.walk_sheet;
        let raccoon = &mut self.raccoon_sheet;
        let megaman = &mut self.megaman_sheet;

        if self.anim == 0.0 {
            self.anim = 10.0
        } else {
            self.anim = 0.0;
        }



        self.gl.draw(args.viewport(), | c, g | {
            clear(GREEN, g);

            rectangle(RED, square, c.transform, g);

            ball.render(c.transform, g, &args.ext_dt);
            ball2.render(c.transform, g, &args.ext_dt);
            walk.render(c.transform, g, &args.ext_dt);
            raccoon.render(c.transform, g, &args.ext_dt);
            megaman.render(c.transform, g, &args.ext_dt);
        });
    }

    fn update(&mut self, args: &UpdateArgs) {
        let ball = &mut self.spritesheet;
        let ball2 = &mut self.spritesheet_two;
        let walk = &mut self.walk_sheet;
        let walk_x = self.walk_x.clone();
        let raccoon = &mut self.raccoon_sheet;
        let racc_x = self.racc_x.clone();
        let megaman = &mut self.megaman_sheet;
        let mega_x = self.mega_x.clone();
        let mega_y = self.mega_y.clone();
        let mega_vel = self.mega_vel.clone();

        ball.set_frame_size(10.0, 10.0);
        ball.set_frame_view(0.0, self.anim);
        ball.set_scale(4.0, 4.0);
        ball.set_pos(20.0, 20.0);

        ball2.set_frame_size(10.0, 10.0);
        ball2.set_frame_view(0.0, 10.0);
        ball2.set_scale(2.0, 2.0);
        ball2.set_pos(40.0, 40.0);

        walk.set_frame_size(95.17, 150.0);
        walk.set_frame_view(0.0, 0.0);
        walk.set_scale(3.0, 3.0);
        if (walk_x >= 1024.0) {
            self.walk_x = -190.0;
        } else {
            self.walk_x = walk_x + 1.0;
        }
        walk.set_pos(self.walk_x, 60.0);

        raccoon.set_frame_size(320.0, 140.0);
        raccoon.set_frame_view(0.0, 0.0);
        raccoon.set_scale(1.0, 1.0);
        if (racc_x >= 1024.0) {
            self.racc_x = -400.0;
        } else {
            self.racc_x = racc_x + 2.0;
        }
        raccoon.set_pos(self.racc_x, 410.0);

        megaman.set_frame_size(70.0, 70.0);
        megaman.set_frame_view(0.0, 0.0);
        megaman.set_scale(2.0, 2.0);

        if (mega_x >= 1024.0) {
            self.mega_x = -70.0;
        }

        if (mega_y >= 600.0) {
            if(!self.dirty) {
                megaman.play("idle");
                self.dirty = true;
            }
        } else {
            megaman.play("jump");
            self.mega_y = mega_y + 4.0;
            self.mega_x = mega_x;
        }

        self.mega_x += self.mega_vel;

        megaman.set_pos(self.mega_x, self.mega_y);
    }

    fn press(&mut self, args: &Button) {
        if let &Button::Keyboard(key) = args {
            match key {
                Key::Left => {
                    self.mega_vel = -3.0;
                    self.megaman_sheet.play("walk");
                    self.megaman_sheet.set_orientation_h(Orientation::Flipped);
                }
                Key::Right => {
                    self.mega_vel = 3.0;
                    self.megaman_sheet.play("walk");
                    self.megaman_sheet.set_orientation_h(Orientation::Normal);
                }
                Key::LCtrl => {
                    self.megaman_sheet.set_orientation(Orientation::Flipped, Orientation::Flipped);
                }
                Key::RCtrl => {
                    self.megaman_sheet.set_orientation(Orientation::Normal, Orientation::Normal);
                }
                _ => {}
            }
        }
    }

    fn release(&mut self, args: &Button) {
        if let &Button::Keyboard(key) = args {
            match key {
                _ => {
                    self.mega_vel = 0.0;
                    self.megaman_sheet.cancel(None);
                    self.megaman_sheet.play("idle");
                }
            }
        }
    }
}

fn main() {
    let opengl = OpenGL::V3_2;

    let mut window: Window = WindowSettings::new(
        "demo  spritesheet",
        [1024, 768]
    )
    .opengl(opengl)
    .vsync(true)
    .exit_on_esc(true)
    .build()
    .unwrap();

    // testing
    let mut sprite_sheet = spritesheet::SpriteSheet::new(Path::new("assets/sprites/ballsheet.png"));
    let mut walk_sheet = spritesheet::SpriteSheet::new(Path::new("assets/sprites/man_walk_right.png"));
    let mut raccoon_sheet = spritesheet::SpriteSheet::new(Path::new("assets/sprites/raccoon_walk.png"));
    let mut megaman_sheet = spritesheet::SpriteSheet::new(Path::new("assets/sprites/megaman_spritesheet_two.png"));

    let first_anim = Animation::new("first".to_string(), 30, vec![(0.0, 0.0), (10.0, 0.0), (0.0, 10.0), (10.0, 10.0)]);
    let walk_anim = Animation::new("walk".to_string(), 60, vec![
        // (0.0, 0.0),
        // (0.0, 0.0),
        // (0.0, 0.0),
        // (0.0, 0.0),
        // (95.17, 0.0),
        // (95.17, 0.0),
        // (95.17, 0.0),
        // (190.34, 0.0),
        // (190.34, 0.0),
        // (190.34, 0.0),
        // (190.34, 0.0)
        (0.0, 0.0),
        (0.0, 0.0),
        (0.0, 0.0),
        (0.0, 0.0),
        (95.17, 0.0),
        (95.17, 0.0),
        (95.17, 0.0),
        (95.17, 0.0),
        (190.34, 0.0),
        (190.34, 0.0),
        (190.34, 0.0),
        (190.34, 0.0),
        (285.51, 0.0),
        (285.51, 0.0),
        (285.51, 0.0),
        (285.51, 0.0),
        (380.68, 0.0),
        (380.68, 0.0),
        (380.68, 0.0),
        (380.68, 0.0),
        (475.85, 0.0),
        (475.85, 0.0),
        (475.85, 0.0),
        (475.85, 0.0),
        (571.02, 0.0),
        (571.02, 0.0),
        (571.02, 0.0),
        (571.02, 0.0),
        (663.19, 0.0),
        (663.19, 0.0),
        (663.19, 0.0),
        (663.19, 0.0),
        (758.36, 0.0),
        (758.36, 0.0),
        (758.36, 0.0),
        (758.36, 0.0),
        (853.53, 0.0),
        (853.53, 0.0),
        (853.53, 0.0),
        (853.53, 0.0),
        (948.70, 0.0),
        (948.70, 0.0),
        (948.70, 0.0),
        (948.70, 0.0),
        (1043.87, 0.0),
        (1043.87, 0.0),
        (1043.87, 0.0),
        (1043.87, 0.0),
    ]);

    let raccoon_anim_data = Animation::load_from_json("assets/data/raccoon.animations.json");
    let megaman_anim_data = Animation::load_from_json("assets/data/megaman.animations.json");

    sprite_sheet.add_animation(first_anim);
    // sprite_sheet.play("first".to_string());

    walk_sheet.add_animation(walk_anim);
    walk_sheet.play("walk");

    // raccoon_sheet.add_animation(raccoon_walk_anim);
    raccoon_sheet.add_animations(&mut raccoon_anim_data.unwrap());
    raccoon_sheet.play("walk");

    megaman_sheet.add_animations(&mut megaman_anim_data.unwrap());
    megaman_sheet.play("jump");
    // endTesting

    let mut app = App {
        gl: GlGraphics::new(opengl),
        spritesheet: spritesheet::SpriteSheet::new(Path::new("assets/sprites/ballsheet.png")),
        spritesheet_two: sprite_sheet,
        walk_sheet: walk_sheet,
        raccoon_sheet: raccoon_sheet,
        megaman_sheet: megaman_sheet,
        mega_x: 400.0,
        mega_y: -70.0,
        mega_vel: 0.0,
        racc_x: 0.0,
        walk_x: 0.0,
        anim: 10.0,
        dirty: false,
    };


    let mut events = Events::new(EventSettings::new());

    while let Some(e) = events.next(&mut window) {
        if let Some(r) = e.render_args() {
            app.render(&r);
        }

        if let Some(u) = e.update_args() {
            app.update(&u);
        }

        if let Some(p) = e.press_args() {
            app.press(&p);
        }

        if let Some(r) = e.release_args() {
            app.release(&r);
        }
    }
}
